%%
% FYP Vishal Khanna
% Script to design guitar loop filter and inverse filter recording
% based on Karjalainen paper
% link to paper: http://users.spa.aalto.fi/vpv/publications/icmc93-guitar.pdf 


% high level algorithm steps:
% 1) Compute the short-time Fourier transform (STFT) of the guitar sound to 
%    be resynthesized

% 2) Measure the magnitude of each detectable harmonic
%    in the STFT frames and form a sampled
%    envelope curve for every harmonic

% 3) Fit a straight line on a logarithmic (dB) amplitude
%    scale to each sampled envelope curve;

% 4) Compute the corresponding loop gain for each slope

% 5) Design a digital filter to match the magnitude
%    spectrum that is formed by the collection of loop
%    gain estimates at different frequencies.

% 6) inverse filter the recording via inverting the transfer function of the loop
%    A(z) = 1/S(z) = 1- H(z)z^-L



%% 1) Compute the short-time Fourier transform (STFT)

clear all
%close all
for i = 1:2
    figure(i)
    cla
end
%clc

%how many harmonics to analyze
%20 harmonics analyzes up to ~6KHz
harmonics = 20;

%sampling freq
fs = 44100;

% note fundamental frequency (estimated by ear)
f = 337;

%frequencies to analyze for stft
frequencies = f:f:harmonics*f;

%load in the waveform
guitar_waveform = audioread('guitar1.wav');

%differentiate the waveform to apply pre-emphasis
diffed_waveform = diff(guitar_waveform);

%compute STFT 
%this window size puts lowest detectable freq at (fs/2048) = 20Hz, low
%enough for our purposes... I am a little unsure here
window_size = 2048;
window = hamming(window_size);


%computing the stft
[s, ~, t] = spectrogram(diffed_waveform, window, window_size/2, frequencies, fs);

%plot of spectrogram, unused, only for visual
figure(1)
spectrogram(diffed_waveform, window, window_size/2, f, fs);
title('Spectrogram of Recorded Guitar Audio (f = 333 Hz)')
colorbar('off')


%% 2) Measure the magnitude of each detectable harmonic
%    in the STFT frames and form a sampled
%    envelope curve for every harmonic

%need magnitude, not complex representation
s = abs(s)';

%get size
[m, n] = size(s);

%to dB
s = arrayfun(@(x) 20*log10(x), s);


%% % 3) Fit a straight line on a logarithmic (dB) amplitude
%       scale to each sampled envelope curve;


%initialize 
gradients = zeros(1,harmonics);

%for each harmonic
for i = 1:harmonics

    %y values are the log of the mag
    y_vals = s(:, i)';

    %fit straight (poly order 1) line to y_vals, t vector
    m = polyfit(y_vals, t, 1);
    
    %grab slope
    gradients(i) = m(1);
end

%% 4) Compute the corresponding loop gain for each slope
% 

gains = zeros(1,harmonics);
L = (fs/f)^-1; %as per paper, unsure why inverted
H = (window_size/2)/fs; 

beta_k = gradients(1);
gains(1) = 10^(beta_k*(L)/(20*H));

for i = 2:harmonics
    beta_k = gradients(i);
    gains(i) = 10^(beta_k*(L)/(20*H)); 
end


%% 5) Design a digital filter to match the magnitude
%    spectrum that is formed by the collection of loop
%    gain estimates at different frequencies.

%since we are using inv freqz, we must estimate the gains for harmonics
%above the 10th (otherwise we end up with a brickwall filter)

%here they are estimated to decay at linear pace

step=2*pi*f/fs;

first=2*pi*(f*(harmonics+1))/fs;

i=first:step:pi;

st = 0.95;
nd = 0.2;

steps = (st-nd)/(length(i)-1);

other_gains=st:-steps:nd;

[B, A] = invfreqz([gains, other_gains], [2*pi*frequencies/fs, i], 1, 1);

%% % 6) inverse filter the recording via inverting the transfer function of the loop


%    A(z) = 1/S(z) = 1 - H(z)z^-L = (Hd(z) - Hn(z)z^-L)/Hd(z)
L = floor(fs/f);
Hn =B;
Hd = A;
numerator = [Hd, zeros(1, L - length(Hd)), -Hn];
denominator = Hd;
excitation_waveform = filter(numerator, denominator, guitar_waveform);

%truncate for initial attack/response only, 50-100ms
beg = 105000;
dist = 4410;
nd = beg+dist;
e = excitation_waveform(beg:nd)';




